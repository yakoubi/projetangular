import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
const httpOption = {
    headers: new HttpHeaders({
        "Access-Control-Allow-Methods": "GET,POST",
        "Access-Control-Allow-Headers": "Content-type",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
    })
};
let AuthentificationService = class AuthentificationService {
    constructor(http) {
        this.http = http;
        this.user = new BehaviorSubject(undefined);
        this.baseURL = "http://localhost:8888/";
    }
    getUser() { return this.user; }
    connect(data) { this.user.next(data); }
    disconnect() { this.user.next(null); }
    verificationConnexion(identifiants) {
        return this.http.post(this.baseURL + 'membre/connexion', JSON.stringify(identifiants), httpOption);
    }
    creerCompte(utilisateur) {
        console.log("utlisateur:" + JSON.stringify(utilisateur));
        return this.http.post(this.baseURL + 'membre', JSON.stringify(utilisateur), httpOption);
    }
};
AuthentificationService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], AuthentificationService);
export { AuthentificationService };
//# sourceMappingURL=authentification.service.js.map