import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let ProduitsComponent = 
/* export class ProduitsComponent {
    private produits:Object[] = [{'nom':'p1', 'qte': 15}, {'nom':'p2', 'qte' :20}];
 */
class ProduitsComponent {
    constructor(activatedRoute, router, authService, produitsService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authService = authService;
        this.produitsService = produitsService;
        this.produits = new Array();
        this.user = this.authService.getUser();
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            if (params["categorie"] !== undefined) {
                this.produitsService.getProduitsParCategorie(params["categorie"]).subscribe(produits => {
                    this.produits = produits;
                    console.log("ICIIIIII" + JSON.stringify(this.produits));
                });
            }
            else {
                this.produitsService.getProduits().subscribe(produits => {
                    this.produits = produits;
                });
            }
        });
    }
    /**ajouter ce produit a un panier */
    ajouterProduitPanier(produitId) {
        console.log("ajouterProduitPanier produiproduitId:" + produitId);
        this.action = "add";
        this.router.navigate(['/panier', this.action, produitId]);
    }
};
ProduitsComponent = tslib_1.__decorate([
    Component({
        selector: 'app-produits',
        templateUrl: './produits.component.html',
        styleUrls: ['./produits.component.css']
    })
    /* export class ProduitsComponent {
        private produits:Object[] = [{'nom':'p1', 'qte': 15}, {'nom':'p2', 'qte' :20}];
     */
], ProduitsComponent);
export { ProduitsComponent };
//# sourceMappingURL=produits.component.js.map