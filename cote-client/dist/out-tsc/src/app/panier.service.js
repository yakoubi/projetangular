import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
let PanierService = class PanierService {
    constructor(http) {
        this.http = http;
        this.urlBase = 'http://localhost:8888/';
        // Http Headers
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
    getPanierParMembre(membre) {
        return this.http.get(this.urlBase + 'panier/' + membre);
    }
    /**
     *
     * @param email
     */
    getPanierProductIds(email) {
        let url = this.urlBase + 'panierProduitIds/' + email;
        let obs = this.http.get(url);
        return obs;
    }
    /**lister tout les produit d'un palier d'un clients */
    getPanierProducts(parametres) {
        let url = this.urlBase + "paniers/produits/" + parametres;
        //let obs :Observable<any> = this.http.get(url).map((res:Response)=> res.json());
        let obs = this.http.get(url);
        console.log("panier production par mail" + JSON.stringify(obs));
        return obs;
    }
    /**ajouter /supprimer un produit du panier */
    modifierPanier(action, produitId, email) {
        // let headers = new Headers({"Contet-type":"application/json"});
        // let options = new RequestOptions({headers:headers});
        let observable;
        //ajouter un produit
        if (action == "add") {
            observable = this.http.post(this.urlBase + "paniers", { "produitId": produitId, "email": email }, this.httpOptions);
        }
        // supprimer un produit:
        if (action == "remove") {
            observable = this.http.delete(this.urlBase + "paniers/" + produitId + "/" + email, this.httpOptions);
        }
        return observable;
    }
};
PanierService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], PanierService);
export { PanierService };
//# sourceMappingURL=panier.service.js.map