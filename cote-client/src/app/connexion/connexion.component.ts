import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from '../authentification.service';


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent {

  private utilisateur = {"email":"", "password": ""};
  private message: string = "";

  constructor(private authService: AuthentificationService,
              private router: Router) { } 


  onSubmit() {
    this.authService.verificationConnexion(this.utilisateur).subscribe(reponse => {
  
    this.message = reponse['message'];
      console.log("verificationConnexion:"+JSON.stringify(reponse));
      console.log(reponse['resultat']);
      if (reponse['resultat']) {
        console.log("yes1")
         this.authService.connect(this.utilisateur.email);
         this.router.navigate(['/categories']);
      } 
      console.log("yes2");
     setTimeout( () => { this.router.navigate(['/categories']); }, 1000 );  
   });   
 }

/** redirection vers le composant add membre */
 creerCompte(){
   this.router.navigate(['/add-membre']);
 }
}
