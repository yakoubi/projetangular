import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IzanComponent } from './izan.component';

describe('IzanComponent', () => {
  let component: IzanComponent;
  let fixture: ComponentFixture<IzanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IzanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IzanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
