
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProduitsComponent } from './produits/produits.component';
import { PanierComponent} from './panier/panier.component';
import { AddMembreComponent } from './add-membre/add-membre.component';
import { IzanComponent } from './izan/izan.component';

const routes: Routes = [
  {
    path:'add-membre',
    component : AddMembreComponent

  },
  { path: 'membres/connexion',
    component: ConnexionComponent
  },
  { path: 'categories',
    component: CategoriesComponent
  },
  { path: 'produits/:categorie',
    component: ProduitsComponent 
  },
  { path: 'produits',
    component: ProduitsComponent
  },
  {
    path: 'panier',
    component: PanierComponent
  },
  {
path:'panier/:action/:produitId',
component: PanierComponent
  },
  {
    path: 'izan',
    component:IzanComponent
  }

  
  
];
// on integre les routes dans l'appli
@NgModule({
imports: [RouterModule.forRoot(routes)], //ici on dit au module router d'angular que toute les routes
                                        //  que tu va enregisterer se trouve dans la constante route
exports: [RouterModule]
})
export class AppRoutingModule { }
