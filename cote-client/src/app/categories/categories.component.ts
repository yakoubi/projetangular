import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ProduitsService } from '../produits.service';
import { Observable } from 'rxjs';
import { AuthentificationService } from '../authentification.service';
import { PanierService } from '../panier.service';
import { __values } from 'tslib';
import{ map} from 'rxjs/operators';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  private user: Observable<string>;
  private categories: String[] = new Array();
  private panier: Object

  constructor(private router: Router,
              private authService: AuthentificationService,
              private produitsService: ProduitsService,
              private panierService: PanierService) {
    this.user = this.authService.getUser();
  }
  ngOnInit() {
    this.produitsService.getCategories().subscribe(categories => {
      this.categories = categories;
    });
console.log("user current:"+this.user['_value']);
/* //
let obs : Observable<any>= this.panierService.getPanierProductIds(this.user['_value']);
      console.log("ids product panier: "+ JSON.stringify(obs));
   */ 
  var sub =  this.panierService.getPanierProducts(this.user['_value']).subscribe(
    value => console.log("mes valeur:"+JSON.stringify(value)),
   err=> console.log("err:"+err));
  }
  
produitsParCategorie(categorie) {
  console.log("mon param ds ecran categories:"+ categorie);
  this.router.navigate(['/produits',categorie]);
  }


  getPanierProducts(){
   var sub =  this.panierService.getPanierProducts(this.user['_value']).subscribe(
     value => console.log("mes valeur:"+value),
   err=> console.log("err:"+err));
   console.log("panier: "+ JSON.stringify(sub));
  }
}
