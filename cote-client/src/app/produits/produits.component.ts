import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router,Params} from  '@angular/router'; 
import { ProduitsService } from '../produits.service'; 
import { Observable } from 'rxjs';
import { AuthentificationService } from '../authentification.service';



@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})
/* export class ProduitsComponent {
    private produits:Object[] = [{'nom':'p1', 'qte': 15}, {'nom':'p2', 'qte' :20}];
 */

export class ProduitsComponent implements OnInit {

    private user: Observable<string>;
    private produits: Object[] = new Array();
    private action: string;
  
    constructor(private activatedRoute: ActivatedRoute,
                    private router :Router,
                private authService: AuthentificationService,
                private produitsService: ProduitsService) {
      this.user = this.authService.getUser();
    }

    ngOnInit() {
      this.activatedRoute.params.subscribe((params :Params) => {    
           if (params["categorie"] !== undefined) {
              this.produitsService.getProduitsParCategorie(params["categorie"]).subscribe(produits => {
                   this.produits = produits;
                   console.log("ICIIIIII"+JSON.stringify(this.produits))
              });
           }
           else {               
              this.produitsService.getProduits().subscribe(produits => {
                   this.produits = produits;
                });
           }
      });
    }
/**ajouter ce produit a un panier */
    ajouterProduitPanier(produitId){
         console.log("ajouterProduitPanier produiproduitId:" + produitId)
        this.action = "add";
     this.router.navigate(['/panier',this.action,produitId]);
    }

    


}
