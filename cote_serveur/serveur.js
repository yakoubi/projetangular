const express = require('express');
const app     = express ();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(function (req,res, next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods', 'Get, POST, PUT,DELETE');// 4 types d'actions demandées au serveur: reuperer R, créér C,mettre à jour U et supprimer DS
     res.setHeader('Access-Control-Allow-Headers', '*');
    next();

});
// app.use(require("cors")); (méthode alternative)

const MongoClient = require ('mongodb') .MongoClient;
const ObjectID = require('mongodb') .ObjectId;
const url     = "mongodb://localhost:27017";

MongoClient.connect(url, {useNewUrlParser: true},(err, client) =>  {
    let db = client.db("SUPERVENTES");

    /* Liste des produits */

    app.get("/produits", (req,res) => {
        console.log("produits");
        try{
            db.collection("produits").find().toArray((err, documents) => {
            res.end(JSON.stringify(documents));
           });
        }catch(e){
            console.log("Erreur sur /produits : " + e);
            res.end(JSON.stringify([]));// renvoie une liste vide
        }
     });

     /** produit par id  */
      app.get("/produit/:id",(req,res)=> {
          let id = req.params.id ;
          let json = JSON.stringify([]);
          console.log("***********************")
          if(/[0-9a-f]{24}/.test(id)){
              db.collection("produits").find({"_id":ObjectID(id)})
                                       .toArray(function(err,documents){
            if(documents !== undefined || documents[0] !== undefined){
json = JSON.stringify(documents[0]);
            }
            console.log("************"+json);
             res.end(json);  
                                       })
          }
      });

      /**liste produit par catégorie */
    app.get("/produits/:categorie", (req,res) => {
        let categorie = req.params.categorie;
        console.log("/produits/"+categorie);
        try{
           db.collection("produits").find({type:categorie}).toArray((err, documents) => {
               res.end(JSON.stringify(documents));
           });
        }catch(e) {;
           console.log("erreur sur /produits/"+categorie+" : "+ e);
        res.end(JSON.stringify([]));
        }
});
    /* liste des categories des produits*/

    app.get("/categories", (req,res) => {
        console.log("/categories");
        categories = [];
        try {
            db.collection("produits").find().toArray((err, documents) => {
                for(let doc of documents) {
                    if (!categories.includes(doc.type)) categories.push(doc.type);
                }
                console.log("Renvoi de"+JSON.stringify(categories));
                res.end(JSON.stringify(categories));
            });
        } catch (e) {
            console.log("Erreur sur /categories: " + e);
            res.end(JSON.stringify([]));
        }

    });
    /* Connexion*/

    app.post("/membre/connexion", (req,res) => {
        console.log("$$$$/utilisateurs/connexion avec "+JSON.stringify(req.body));
        try{
            db.collection("membres")
            .find(req.body)
            .toArray((err, documents) => {
              if (documents.length == 1)
                 res.end(JSON.stringify({"resultat": 1, "message": "Authentification reussie"}));
             else res.end(JSON.stringify({"resultat": 0, "message": "Email et/ou mot de pass incorrect"}));
            });
        } catch (e) {
            res.end(JSON.stringify({"resultat": 0, "message": e}));
        }

    });
    // creer un compte utilisateur ajouter l'utlisateur a la table membre 

    app.post("/membre", (req,res)=>{
        console.log("body:"+ JSON.stringify(req.body));
        try{
            db.collection("membres")
            .insert(req.body);
            //creer un panier
            db.collection("paniers").insert({
                "email":req.body.email,
                "order":[ ],
                "quantite":"",
                "totalAPayer":""
            });
           res.end(JSON.stringify({"resultat": 1, "message": "compte creé"}));
        } catch (e) {
            res.end(JSON.stringify({"resultat": 0, "message": e}));
        }

    });

    /* liste des identifiants des produits du panier d'un client*/

 app.get("/panierProduitIds/:email",function(req,res){
let email = req.params.email;
console.log("url"+"/panier/produitIds/"+email);
db.collection("paniers").find({"email":email}).toArray(function(err, documents){
    if (err) {
        console.log("ici")
        res.end(JSON.stringify({"resultat": 0, "message": e}));
 
    } 
    if(documents !== undefined || documents[0] !== undefined){
        console.log("doc:"+JSON.stringify(documents));
let order = documents[0].order;  
res.setHeader("Content-type","application/json,charset=UTF-8");
let json = JSON.stringify(order);
res.end(json);
    }
console.log("la")
});
 });

//lister tous les produits du panier d'un client
app.get("/paniers/produits/:email",function(req,res){
let email = req.params.email;
let pipeline =[
    {$match:{"email": email}},
    {$unwind: "$order"},

    {$lookup:{from: "produits",
              localField: "order",
              foreignField: "_id",
              as:"produit"}},

   {$unwind: "$produit"},
   {$group: {"_id": "$_id",
            "order": {"$push":"$order" },
            "produits": {"$push": "$produit"}
        }
    }  

];
db.collection("paniers").aggregate(pipeline).toArray(function(err,documents){
    let json;
    console.log("panier"+ JSON.stringify(documents));
    if (documents !== undefined && documents[0] !==undefined){
        let produitsInE =documents[0].produits;
        let produitsInI = {};
        for(let produit of produitsInE){
            if (produit._id in produitsInI) 
            produitsInI[produit._id].nb++;
            else 
            produitsInI[produit._id] = {"_id": produit._id,
                                            "type": produit.type,
                                            "brand": produit.brund,
                                            "nom": produit.nom,
                                            "modele": produit.modele,
                                            "prix": produit.prix,
                                            "quantite":1,
                                            "image":produit.image   }

        }
        let produitList = [];
        for (let produitId in produitsInI){
            produitList.push(produitsInI[produitId]);

        }
        json=JSON.stringify(produitList);

    }
    else json=JSON.stringify([]);
    res.setHeader("Contet-type","application/json; charset=UTF-8");
    console.log("mon panier:"+ json);
    res.end(json);
});
});

 //ajouter / supprimer un produit du panier

 app.post('/paniers',function(req,res){
let email = req.body.email;
let produitId = req.body.produitId;
console.log("============"+email + produitId);
db.collection("paniers").find({"email":email}).toArray( function(err,documents){
    let  json;
    if (documents !== undefined && documents[0] !== undefined) {
        let order = documents[0].order;
        order.push(ObjectID(produitId));
        db.collection("paniers").update(
            {"email":email}, 
            {$set: {"order":order}}
            );
            json = JSON.stringify(order);
    }
    else json = JSON.stringify([]);
    res.setHeader("Contet-type","application/json; charset=UTF-8");

    res.end(json);
})



 });

 
app.delete("/paniers/:produitId/:email", function(req,res){
    let email = req.params.email;
    let produitId = req.params.produitId;
    console.log("delete:"+email + produitId);
     db.collection("paniers").find({"email":email}).toArray(function(err,documents){
         let json;
         if (documents !== undefined && documents[0] !== undefined) {
             let order = documents[0].order;
             let position = order.map(function(e){
                 return e.toString();
             }).indexOf(produitId);
             if(position != -1){
                 order.splice(position,1);
                 db.collection("paniers").update({"email":email},
                                                 {$set:{"order":order}

                                                    }); 
                json = order;         
             }
         }
         res.setHeader("Contet-type","application/json; charset=UTF-8");
res.end(JSON.stringify(json));
     });
});




 });


 
app.listen(8888);